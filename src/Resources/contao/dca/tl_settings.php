<?php
  $GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] = str_replace(
    '{files_legend:hide}',
    '{files_legend:hide},always_load_lazy',
    $GLOBALS['TL_DCA']['tl_settings']['palettes']['default']
  );

  $GLOBALS['TL_DCA']['tl_settings']['fields']['always_load_lazy'] = array(
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'long')
  );
?>
