<?php

namespace Contao;

class TemplateImage extends ContentImage
{
	protected $strTemplate = 'template_image';

	public static function buildImage($image,$size){
		$Image = new self(ContentModel::findAll(['limit'=>1]));
		$Image->type = 'image';
		$Image->singleSRC = $image;
		$Image->size = $size;
		return $Image->generate();
	}
}
