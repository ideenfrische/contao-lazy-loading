<?php
/**
 * SimpleAjax extension for Contao Open Source CMS
 *
 * Copyright (c) 2016-2018 Richard Henkenjohann
 *
 * @package SimpleAjax
 * @author  Richard Henkenjohann <richardhenkenjohann@googlemail.com>
 */


namespace ideenfrische\LazyLoadingBundle;


use Symfony\Component\HttpKernel\Bundle\Bundle;

class ideenfrischeLazyLoadingBundle extends Bundle
{

}
